# Документация nftables в примерах

## Планирую тут, выкладывать тектовые версии примеров, которые буду объяснять в видео.

Телеграм чат, по теме nftables
[LinuxToDmitry-nftables](https://t.me/+nEvf5alrax8yNTIy)

**Плей листы** \
[Youtube](https://www.youtube.com/playlist?list=PLOp6AT9LJS_OeWLu1cXIrZbqb1Mnnu1vl),
[Дзен](https://dzen.ru/suite/d3c2df91-bf2e-445f-b679-fb39b0787080),
[RUTUBE](https://rutube.ru/plst/378987)

**Плей листы, Port knocking на чистом nftables** \
[Youtube](https://www.youtube.com/playlist?list=PLOp6AT9LJS_M6vW6jebKptQcZOaID_x1V),
[Дзен](https://dzen.ru/suite/b5eccad7-241b-4968-9210-783758f36a3a),
[RUTUBE](https://rutube.ru/plst/377922)

**Nftables** совсем не простая тема, я хоть и долго уже им занимаюсь, но примерно половину, а может и больше, не знаю его возможностей.
Однако, это не повод паниковать, т.к. базовые возможности **nftables** очень простые и на мой взгляд, проще чем **iptables**.

В **nftables**, есть новые возможности, которые упрощают написание правил для базового понимания, но так же есть и более серьёзные и сложные решения, которых не было в **iptables**.
Не думаю, что мы когда нибудь будем массово применять все возможности **nftables**, в большинстве задачь они вам просто не пригодятся, но знать о них, было бы круто).

На самом деле, мне кажется такой вариант обучения, будет более понятен новичкам.
На мой взгляд, важно первым делом показать из чего состоит **nftables** конфиг и уже потом рассказывать как строятся эти правила в терминале.

**Файл: nftables.conf.clean** \
Базовый пример, в котором я показал основные компоненты для постройки правил **nftables**, этого примера достаточно, для какого нибудь шлюза или VDS.

**Файл: nftables.conf.clean.all** \
Базовый пример, со всеми таблицами и цепочками которые возможно настроить в **nftables**. Возможно я чего-то не учёл, возможно со временем будут дополнения от команды разработчиков **nftables**.
Возможно в коментариях или в чате вы меня ещё поправите и мы вместе составим полную картину в понимании **nftables**, я надеюсь на всех нас).

> Кликните на сам файл выше, что бы посмотреть его содержимое.

## Небольшая памятка
В Debian и Ubuntu, конфиг находится тут
```
/etc/nftables.conf
```
Во всех остальных, придётся самостоятельно искать, например посмотреть в службе **nfatbles.service**

Применить правила из файла в ручную.
```
nft -f /etc/nftables.conf
```

> **Nftables**, сначала проверяет файл и уже потом применяет в случае если нет ошибок, в противном случае, покажет ошибку и оставит старые правила.

### Тип данных icmp
Запрос IPv4 | Код | | Запрос IPv6 | Код 
-|-|-|-|-|
echo-reply              | 0 | | destination-unreachable | 1
destination-unreachable | 3 | | packet-too-big | 2
source-quench           | 4 | | time-exceeded | 3
redirect                | 5 | | parameter-problem | 4
echo-request            | 8 | | echo-request | 128
router-advertisement    | 9 | | echo-reply | 129
router-solicitation     | 1 | | mld-listener-query | 130
time-exceeded           | 11 | | mld-listener-report | 131
parameter-problem       | 12 | | mld-listener-done | 132
timestamp-request       | 13 | | mld-listener-reduction | 132
timestamp-reply         | 14 | | nd-router-solicit | 133
info-request            | 15 | | nd-router-advert | 134
info-reply              | 16 | | nd-neighbor-solicit | 136
address-mask-request    | 17 | | nd-redirect | 137
address-mask-reply      | 18 | | router-renumbering | 138
| | | | ind-neighbor-solicit | 141
| | | | ind-neighbor-advert | 142
| | | | mld2-listener-report | 143

> Есть большая вероятность, что табличка не совсем корректна, не помню где я её нашёл, но вроде всё правильно.

## Часть 1

**Видео: nftables часть 1 - Надо было с чего-то начать** \
[Youtube](https://youtu.be/mW8K5Gy12Gc),
[Дзен](https://dzen.ru/video/watch/65e6136a11908123ed6bc740),
[RUTUBE](https://rutube.ru/video/8e62126899c8eab932afc591b8d06aa2/)

## Часть 2

Не большая статья по vmap с примерами \
[Verdict Maps (vmaps)](https://wiki.nftables.org/wiki-nftables/index.php/Verdict_Maps_(vmaps))

**Файл: nftables.conf.vmap**

**Видео: nftables часть 2 - Состояние пакетов invalid, established, related и untracked, плюс vmap.** \
[Youtube](https://youtu.be/m5u7tI76PDc),
[Дзен](https://dzen.ru/video/watch/65e61904228ac73d22f8645e),
[RUTUBE](https://rutube.ru/video/afb74e5dab311bdc944c66ed5286f48c/)

## Часть 3

Статьи из видео \
[Arch manual](https://man.archlinux.org/man/nft.8#ADDRESS_FAMILIES) \
[Netfilter hooks](https://wiki.nftables.org/wiki-nftables/index.php/Netfilter_hooks)

**Видео: nftables часть 3 - Шаблоны и просто поговорим.** \
[Youtube](https://youtu.be/GCuqlrzZBAw),
[Дзен](https://dzen.ru/video/watch/6608db9c72997b276bffdd3a),
[RUTUBE](https://rutube.ru/video/321d774764f426215b73f7b6c53eefdb/)

## Часть 4

**Файл: nftables.conf.inet.force**

Настройка ядра
```
nano /etc/sysctl.conf
```
```
net.ipv4.conf.all.forwarding=1
#net.ipv4.conf.ens18.forwarding=1
#net.ipv4.conf.ens19.forwarding=1
```
Применить настройки ядра
```
sysctl -p
```

**Видео: nftables часть 4 - Настройка ядра и как быстро раздать интернет.** \
[Youtube](https://youtu.be/8tFAOxwYzWI),
[Дзен](https://dzen.ru/video/watch/660c189556b2675a854f3a02),
[RUTUBE](https://rutube.ru/video/4ee4db62ff69224f60cf789e6335d7e0/)

## Часть 5

**Файл: nftables.conf.inet.local1**

**Видео: nftables часть 5 - Type filter hook input, forward.** \
[Youtube](https://youtu.be/fzD_YuWQLHE),
[Дзен](https://dzen.ru/video/watch/660cfc0295bcac1e2c8dc0a8),
[RUTUBE](https://rutube.ru/video/75645273d890c0908c6489b992276ecf/)

## Поговорим

**Видео: nftables - Поговорим** \
[Youtube](https://youtu.be/D6om56_MwWs),
[Дзен](https://dzen.ru/video/watch/660e1918119706162b737ef7),
[RUTUBE](https://rutube.ru/video/b1a3fe43e569d3e7db34df6987bd9aa0/)

## Ответ по DROP IPv6

**Файл: nftables.conf.drop.ipv6**

**Видео: nftables - Ответ по DROP IPv6.**
[Youtube](https://youtu.be/nXu1l9cYd50),
[Дзен](https://dzen.ru/video/watch/660ec3609c7e2d24074e99c6),
[RUTUBE](https://rutube.ru/video/266082a855aff3d58e87d2e710704726/)

## Ответ, почему пакет сначала выходит и потом попадает в input. INPUT, OUTPUT, FORWARD.

**Файл: nftables.conf.input**

Дополнительный материал по сетям и протоколам, плей листы от Георгий Курячий, канал [UNUX](https://www.youtube.com/@unx7784) \
Почему-то не нашёл его на RuTube, вроде видел, скорей всего чьи-то перезаливы. \
[ACBK сети (2021)](https://www.youtube.com/watch?v=E_gA-DehDi4&list=PL6kSdcHYB3x7IaYsg-MaN6vuxq7YVKK7D&pp=iAQB),
[Сетевые протоколы в Linux (весна 2022)](https://www.youtube.com/watch?v=A0FVMTZB6UE&list=PL6kSdcHYB3x6z_j1fcCYhZuS5b1swP4i3&pp=iAQB),
[Сетевые протоколы в Linux (весна 2023)](https://www.youtube.com/watch?v=jBeIwlr3lOc&list=PL6kSdcHYB3x6ocPglvEji5im6mfEjSzZR&pp=iAQB),
[Сетевые протоколы в Linux (весна 2024)](https://www.youtube.com/watch?v=6x82s7I0kyU&list=PL6kSdcHYB3x6dc3vjPfrITqNEQr50l8Kw&pp=iAQB)

**Нарисовал как смог).** \
В данном примере, мы посылаем один icmp пакет от сервера **14** до **56**, через **15**.

На сервере **14**, запрос (**echo-request** или **type 8**) уходит через (**OUTPUT**), проходит транзитом через сервер **15** (**FORWARD**) и приходит на сервер **56** в (**INPUT**). \
Далее сервер **56**, отвечает за запрос (**echo-reply** или **type 0**) через (**OUTPUT**), возвращается так же как и пришёл, через сервер **15** (**FORWARD**) и приходит обратно на сервер **14** (**INPUT**), только уже в виде ответа (**echo-reply** или **type 0**).

<p align="center"><img src="img/netfilter.micro.svg"/></p>

> Зелёная соединительная стрелочка, запрос (**echo-request** или **type 8**), а оранжевая ответ (**echo-reply** или **type 0**)

**Видео: ** \
[Youtube](https://youtu.be/2CpiU2R7YXk),
[Дзен](https://dzen.ru/video/watch/661a3b6ab1e0a65d70de19df),
[RUTUBE](https://rutube.ru/video/dd7836059a30b91c05d886ebfeb59bd8/)
